#!/bin/bash

# [c] 2021 Marcus W., Bonn, Germany
# the file with games and chess moves
# pgn file format
# https://en.wikipedia.org/wiki/Portable_Game_Notation


file='chess.pgn'

    # number of new lines we have passed
    newline=0

    # the counter for each line
    i=0

    #count  how many lines we have?
    lastline=$(wc -l < "$file")

    # do we need to add an other 'game' or do we have the last one?
    maxlines=$((lastline-2))

     while read line || [ -n "$line" ] ; do
        #only do this section th first time
        if [ $i == 0 ]
        then
            jsonInfo=$'[\n'
            jsonInfo+=$'{\n';
            jsonInfo+=$'\t"Type": "Play",\n'
            jsonInfo+=$'\t"Info": {\n'
        fi

        # starts a line with [?
        # if so we have a list of fields
        # first remove the [ and ]
        if [[ "$line" =~ ^\[ ]]
        then
          line=${line//[/}
          line=${line//]/}

          #split on space
          IFS='"' read -ra lineArray <<< "$line"
          trimmed="${lineArray[0]%?}"

          # create a key value pair
          # "Event": "Troll Masters",
          #  "Site": "Gausdal NOR",
          jsonInfo+="\"$trimmed\": \"${lineArray[1]}\","
        fi

        #
        # do we have a new line newline?
        # it is newline nr. 1
        if [ "$line" == "" ]
        then
            # newline++
            newline=$((newline+1))
            # remove the last character (that is a ,) we dont need that here
            jsonInfo="${jsonInfo%?}"
            jsonInfo+=$'\t},\n'
        fi

        # in we have newline nr1,
        # than we come in the section of the moves
         if [ $newline == 1 ]
         then
             if [[ "$line" =~ ^[0-9]\. ]]
             then
                jsonInfo+=$'\t"Movelist":"'
                jsonInfo+="$line\""
                jsonInfo+=$' \n'
            fi
         fi

        # if newline ==2 than we have a new game
        # only when the counter i is msmaller than the maxlines
        if [[ $i -ne $maxlines ]]
        then
            if [ $newline == 2 ]
            then
                jsonInfo+=$'\t{\n';
                jsonInfo+=$'\t"type": "Play",\n'
                jsonInfo+=$'\t"Info": { \n'

                # reset the newline counter
                newline=0
            fi
        fi
    i=$((i+1))


    done < $file


jsonInfo+=$'\t}\n'
jsonInfo+=$']';

# print json to screen
echo "$jsonInfo"

# save json to file
echo  "$jsonInfo" > json.txt
